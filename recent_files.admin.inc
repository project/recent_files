<?php

/**
 * @file
 * Admin-related functions for Recent Files module.
 */

/**
 * Get a list of files that have been modified/created within the given timeframe. 
 * Use that list to create the archive file.
 *
 * @param $days_old 
 *   The number of days to grab files for. Files newer than this value will be included
 *   in the list. Default is 30.
 * @param $filetype
 *   The type of file to generate. Default is tgz.
 *   - Tar/Gzip (.tgz)
 *   - Zip (.zip)
 *
 * @todo Set the archive file name format as a user-configurable variable
 */
function recent_files_get_file_list($days_old = 30, $filetype = 'tgz') {
  $now = time();
  $starttime = $now - ($days_old * 24 * 60 * 60); // get the number of days represented as seconds

  // Get an array of all the files in the files directory newer than our start date.
  $files = recent_files_file_scan_directory($starttime);
  $filelist = $files['filename'];

  // Don't do anything if there are no files newer than the specified time frame.
  if($files['_totalsize'] == 0) {
    drupal_set_message(
      t('There were no files newer than !daycount !days, so no archive file was created.',
      array('!daycount' => $days_old, '!days' => format_plural($days_old, 'day', 'days')))
    );
    return FALSE;
  }

  // Get a decent filename based on the timestamp
  $date = date("m-d-Y-Hi", $now);
  $archive_filename = "files_$date.$filetype";
  
  // Create the archive file
  if ($filetype == 'tgz') {
    include 'Archive/Tar.php';
    $tar = new Archive_Tar(RECENT_FILES_PATH . '/' . $archive_filename, 'gz');
    $tar->create($filelist);
  }
  elseif ($filetype == 'zip') {
    $zip = new ZipArchive();
    if ($zip->open(RECENT_FILES_PATH . '/' . $archive_filename, ZIPARCHIVE::CREATE) === TRUE) {
      foreach ($filelist as $filename) {
        $zip->addFile($filename);
      }
      $zip->close();
    }
  }

  // Record the file in the database.
  // NOTE: This is not currently used. It is intended that additional file informaton will be kept here, such 
  // as when the file was created, how long it will remain before it is deleted, etc. This may not be used at 
  // all, since most of the intended informaton can be derrived easily from the file system.
  if (file_exists(RECENT_FILES_PATH . '/' . $archive_filename)) {
    db_query("INSERT INTO {recent_files} (filename, starttime, endtime) VALUES('%s', %d, %d)", $archive_filename, $now, $starttime); 
    drupal_set_message(t('The archive file <em>%filename</em> was created.', array('%filename' => $archive_filename)), $type = 'status', FALSE);
  }
  else {
    drupal_set_message(t('The archive file <em>%filename</em> was not created.', array('%filename' => $archive_filename)), $type = 'warning', FALSE);
  }
}

/**
 * Scan the files directory to get a list of all files newer than our start time.
 *
 * @param $startime
 *   The time to compare file mtimes against in seconds. Files newer than this value
 *   (in epoch time) will be added to the $files array.
 *
 * @return
 *   Assocative array containing the list of files, along with '_totalsize' which is the size
 *   of all uncompressed files contained in the list. 
 */
function recent_files_file_scan_directory($starttime) {
  $recurse = TRUE;
  $files = array();
  
  // Get a list of file name masks to exclude.
  // TODO: Something better than a newline split to get the array of exclusions
  $nomask_settings = variable_get('recent_files_exclusions', "CVS\ntmp\nimagecache\njs\ncss\nxmlsitemap\nsignwriter-cache\ncache");
  $nomask_settings = preg_split('/(\r\n?|\n)/', $nomask_settings);
  $nomask = array_merge(array('.', '..', 'recent_files'), $nomask_settings);

  // We want to keep track of total file size. Initialize that value to 0 if this is 
  // the first iteration of the function call.
  if (!isset($files['_totalsize'])) {
    $files['_totalsize'] = 0;
  }
  
  // Get list of files to evaluate
  $filelist = file_scan_directory(file_directory_path(), '.*', $nomask);

  // Loop through the directory, calling this function recursively if the file examined
  // turns out to be another directory.
  foreach ($filelist as $file) {
    if (filemtime($file->filename) > $starttime) {
      $size = filesize($file->filename);
      $files['filename'][] = $file->filename;
      $files['filesize'][] = $size;
      $files['_totalsize'] += $size;
    }
  }
  return $files;
}

/**
 * Menu callback function to display the admin UI page. This generates a table of all recent files
 * plus some basic options to create a new archive file.
 *
 * @return
 *   The themed output of the archived file table and archive creation options.
 */
function recent_files_admin() {
  $output = '';
  $rows = array();
  $form = drupal_get_form('recent_files_create_archive_form');
  $files = file_scan_directory(RECENT_FILES_PATH, '.*');
  $header = array(
    t('Filename'),
    t('Created'),
    t('File Size'),
    t('Action'),
  );
  
  if (count($files)) {
    foreach ($files as $file) {
      // Filename
      $filedata = stat($file->filename);
      $rows[] = array(
        l($file->basename, $file->filename),
        format_date($filedata['mtime'], 'large'),
        format_size($filedata['size']),
        l(t('Delete'), 'admin/settings/recent_files/delete/' . $file->basename),
      );
    }
  }
  else {
    $rows[][] = array (
      'data' => t('There are no archive files available to download.'),
      'colspan' => 4,
    );
  }
  
  $output .= theme('table', $header, $rows);
  $output .= $form;
  
  return $output;
}

/**
 * Drupal system settings form.
 *
 * @return
 *   System settings form output.
 */
function recent_files_admin_settings() {
  $form = array();
  
  $form['recent_files_ttl'] = array(
    '#type' => 'select',
    '#title' => t('Time to keep'),
    '#description' => t('How long to keep archive files around. These will be automatically deleted after the time selected.'),
    '#options' => array(
      1 => t('1 Day'),
      3 => t('3 Days'),
      7 => t('1 Week'),
      14 => t('2 Weeks'),
      30 => t('1 Month'),
      90 => t('3 Months'),
      0 => t('Never'),
    ),
      '#default_value' => variable_get('recent_files_ttl', 0),
  );

  $form['recent_files_exclusions'] = array(
    '#type' => 'textarea',
    '#title' => t('Files to exclude'),
    '#default_value' => variable_get('recent_files_exclusions', preg_split('/(\r\n?|\n)/', "CVS\ntmp\nimagecache\njs\ncss\nxmlsitemap\nsignwriter-cache\ncache")),
    '#description' => t("A list of file and/or directory names to exclude from the archive, each on a separate line. Examples include: <em>tmp</em>, <em>imagecache</em>, <em>css</em>, and <em>js</em>."),
  );

  return system_settings_form($form);
}

/**
 * Form to display in the main admin page to generate a new archive file.
 *
 * @return
 *   FAPI form array.
 */
function recent_files_create_archive_form() {
  $form = array();

  $form['recent_files'] = array(
      '#type' => 'fieldset',
      '#title' => t('Create an archive'),
  );

  // The days that files must be newer than in order to be included in the archive.
  $form['recent_files']['days'] = array(
    '#type' => 'select',
    '#title' => t('Number of Days'),
    '#options' => array(
        1       => 1,
        2       => 2,
        5       => 5,
        7       => 7,
        10      => 10,
        14      => 14,
        30      => 30,
        60      => 60,
        90      => 90,
        100000  => 'All',
      ),
    '#description' => t('The number of days\' worth of files to archive.'),
  );
  
  // The desired file type.
  $form['recent_files']['filetype'] = array(
    '#type' => 'radios',
    '#title' => t('File Type'),
    '#options' => array(
        'tgz' => 'tar/gzip',
        'zip' => 'zip',
      ),
    '#default_value' => 'tgz',
    '#description' => t('The format of the archive file (tar/gzip or zip).'),
  );

  $form['recent_files']['submit'] = array(
    '#type' => 'submit',
    '#value' => 'Create archive',
  );

  return $form;
}

/**
 * Form submission handler for the create archives form.
 */
function recent_files_create_archive_form_submit($form, &$form_state) {
  $days = $form_state['values']['days'];
  $type = $form_state['values']['filetype'];
  recent_files_get_file_list($days, $type);
}

/**
 * confirm_delete function
 * Conform that the user wants to delete the given filename
 */
function recent_files_confirm_delete(&$form_state, $fid = 0) {
  // The filename is passed on the URL as an arguement. Clean it up.
  // NOTE: The intention is that eventually file information will be stored in the database
  // and we can pass the file ID instead of a filename.
  $filename = check_plain(arg(4));
  
  if (file_exists(RECENT_FILES_PATH . '/' . $filename)) {
    $form = array();
    $form['#fid'] = $fid;
    return confirm_form(
      $form,
      t('Are you sure you want to delete the archive file %filename?', array('%filename' => $filename)),
      'admin/settings/recent_files',
      t('You are about to delete the archive file %filename. This action cannot be undone.', array('%filename' => $filename)), t('Delete')
    );
  }
  else {
    drupal_set_message(t('That archive file does not exist.'), $type = 'error', FALSE);
    drupal_goto('admin/settings/recent_files');
  }
}

/**
 * form submit handler for the confirm delete form.
 * Handles deletion of the specified archived file.
 */
function recent_files_confirm_delete_submit($form, &$form_state) {
  // The filename is passed on the URL as an arguement. Clean it up.
  // NOTE: The intention is that eventually file information will be stored in the database
  // and we can pass the file ID instead of a filename.
  $filename = check_plain(arg(4));
  if (file_exists(RECENT_FILES_PATH . '/' . $filename)) {
    unlink(RECENT_FILES_PATH . '/' . $filename);
    drupal_set_message(t('The archive file %filename was deleted.', array('%filename' => $filename)), $type = 'status', FALSE);
    drupal_goto('admin/settings/recent_files');
  }
  else {
    drupal_set_message(t('That archive file does not exist.'), $type = 'error', FALSE);
    drupal_goto('admin/settings/recent_files');
  }
}